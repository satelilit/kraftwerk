# Kraftwerk

## Go sample code

### Arch
![overview arch](./arch.png "architecture")

### Prerequisites
- Go
- MySQL
- .env file

### .env content
- `DB_USER` for Database Username
- `DB_PASS`  for Database Password
- `DB_IP` for Database IP 

For local use, you can spin the mysql using `docker-compose` on `compose` directory. Ensure that you have `docker` and `docker-compose` installed on your machine first.

### Docker-compose env
docker-compose on `compose` folder needs .env file too. It will needs below variable:
- `MYSQL_USER`
- `MYSQL_PASSWORD`
- `MYSQL_ROOT_PASSWORD`