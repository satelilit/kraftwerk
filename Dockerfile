FROM golang:1.17-alpine as builder
WORKDIR /app
COPY .env ./
COPY go.* ./
COPY *.go ./
RUN go build 

FROM golang:1.17-alpine
WORKDIR /app
COPY --from=builder /app/kraftwerk ./
COPY --from=builder /app/.env ./
CMD ["/app/kraftwerk"]