package main

import (
	"database/sql"
	"log"
	"github.com/joho/godotenv"
	"os"
)

func loadEnv (key string) string {
   load := godotenv.Load(".env")
   if load != nil {
	   log.Fatalf("Error loading env file")
   }
   return os.Getenv(key)
}

func connect() *sql.DB {

    dbuser := loadEnv("DB_USER")
	dbpass := loadEnv("DB_PASS")
	dbip := loadEnv("DB_IP")
	db, err := sql.Open("mysql", dbuser+ ":" + dbpass + "@tcp("+ dbip + ":3306)/testdb")

	if err != nil {
		log.Fatal(err)
	}

	return db
}